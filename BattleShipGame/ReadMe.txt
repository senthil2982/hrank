									BattleShipGame
						
Requirements: Java 8 or higher compiler, JUnit 5, Maven 3.5.4

Input: The input is read from file "battleshipinput.txt". This is hardcoded in the code and not taken in as argument.

Execute:
	To build, execute - "mvn package" inside the project folder.
	To run the program, execute - "mvn exec:java" inside the project folder.

Output: Output is printed to the standard out.

MainClass: game.BattleShipGame

Assumptions Made:
1. Each player gets separate boards.
2. For the third input line - Q 1 1 A1 B2, the first coordinate(A1) is assumed to be the first player's ship and 
second coordinate(B2) is assumed to be for the second player and third coordinate if any will be for first player's ship in a alternating fashion.



