package game;
import java.io.File;
import java.util.Scanner;

import game.board.Board;
import game.board.Coordinates;
import game.missiles.MissileType;
import game.player.Player;
import game.player.PlayersCircularList;
import game.ship.BattleShip;
import game.ship.BattleShipType;


public class BattleShipGame {

	private PlayersCircularList players = new PlayersCircularList();

	private void readInput(File f)
	{
		Scanner sc;
		try {
			sc = new Scanner(f);
			int width = sc.nextInt();
			String height = sc.next();

			int noOfShips = sc.nextInt();
			//separate boards for each player
			Board board1 = new Board(width,height,noOfShips);
			Board board2 = new Board(width,height,noOfShips);
			Player player1 = new Player("Player-1",board1,noOfShips);
			Player player2 = new Player("Player-2",board2,noOfShips);
			//Players added to a circularlist for easy iteration
			players.add(player1);
			players.add(player2);

			String battleshiptype1 = sc.next();
			int shipWidth1 = sc.nextInt();
			int shipHeight1 = sc.nextInt();
			BattleShipType shipType1 = new BattleShipType(battleshiptype1,shipWidth1,shipHeight1,board1);
			String[] coords1 = sc.nextLine().split(" ");
			/* After the ship width and ship height, the coordinates are assigned to players alternatively*/
			for(int i=1;i<coords1.length;i++)
			{
				Player p = players.getNextPlayer();
				BattleShip shp = new BattleShip(p, shipType1,coords1[i],p.getBattleBoard());
				p.addShip(shp);
			}

			String battleshiptype2 = sc.next();
			int shipWidth2 = sc.nextInt();
			int shipHeight2 = sc.nextInt();
			players.reset();
			BattleShipType shipType2 = new BattleShipType(battleshiptype2,shipWidth2,shipHeight2,board2);
			String[] coords2 = sc.nextLine().split(" ");
			/* After the ship width and ship height, the coordinates are assigned to players alternatively*/
			for(int i=1;i<coords2.length;i++)
			{
				Player p = players.getNextPlayer();
				BattleShip shp = new BattleShip(p, shipType2,coords2[i],p.getBattleBoard());
				p.addShip(shp);
			}
			String[] player1missiles = sc.nextLine().split(" ");
			player1.addMissileSequence(player1missiles);
			String[] player2missiles = sc.nextLine().split(" ");
			player2.addMissileSequence(player2missiles);
			String[] player1Attacks = sc.nextLine().split(" ");
			player1.addAttackSequence(player1Attacks);
			String[] player2Attacks = sc.nextLine().split(" ");
			player2.addAttackSequence(player2Attacks);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void playGame(File f)
	{
		readInput(f);
		PlayersCircularList currentPlayers = players;
		Player winner = null;
		Player currentPlayer = null,opponent = null;
		currentPlayers.reset();
		int playersWithNoMissiles = 0;
		while(currentPlayers.getPlayersSize()!=playersWithNoMissiles)
		{
			if(currentPlayer == null)//Only for the first time
			{
				currentPlayer = currentPlayers.getNextPlayer();
				opponent = currentPlayers.getOpponentPlayer();
			}
			if(currentPlayer.getShips().isEmpty())
			{
				//If one of the player's ships are all destroyed then the other player has won 
				break;
			}
			Coordinates attackCoordinate = currentPlayer.getNextAttackCoordinate();
			MissileType missileType = currentPlayer.getNextMissileType();
			if(attackCoordinate!=null)
			{
				playersWithNoMissiles--;
				boolean successfulHit = opponent.targetShip(attackCoordinate,missileType);
				if(!successfulHit)
				{	 
					System.out.println(currentPlayer.getPlayerName()+" fires a missile of type "+missileType.toString()+" with target "+attackCoordinate.toString()+" which got miss");
					currentPlayer = currentPlayers.getNextPlayer();
					opponent = currentPlayers.getOpponentPlayer();
				}
				else
				{
					System.out.println(currentPlayer.getPlayerName()+" fires a missile of type "+missileType.toString()+" with target "+attackCoordinate.toString()+" which got hit");
					winner = checkWinner();
					if(winner!=null)
					{
						System.out.println(winner.getPlayerName()+" won the battle");
						break;
					}
				}
			}
			else//attackCoordinates are null
			{
				System.out.println(currentPlayer.getPlayerName()+" has no more missiles left to launch" );
				playersWithNoMissiles++;
				currentPlayer = currentPlayers.getNextPlayer();
				opponent = currentPlayers.getOpponentPlayer();
			}
		}
		if(winner == null)//No one is a winner so far after all the missiles have been used
		{
			winner = checkWinner();
			if(winner==null)
			{
				System.out.println("The game is a draw");
			}
			else
			{
				System.out.println(winner.getPlayerName()+" won the battle");
			}
		}

	}

	private Player checkWinner()
	{
		int playersWithShips = 0;
		Player winner = null;
		for(Player p:players.getPlayerList())
		{
			if(!p.getShips().isEmpty())
			{
				playersWithShips++;
				winner = p;
			}
		}
		//If there are more than one player with ships then there is no winner,it's a draw
		// If there is only one player with ships then he is the winner
		if(playersWithShips!=1)
		{
			winner = null;
		}
		return winner;
	}

	public static void main(String[] args) 
	{
		BattleShipGame bsg = new BattleShipGame();
		//Takes the input from the file battleshipinput.txt
		bsg.playGame(new File("battleshipinput.txt"));

	}

}

