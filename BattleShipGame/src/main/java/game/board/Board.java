package game.board;
import java.util.Set;

import game.exceptions.InvalidBoardDimension;
import game.exceptions.InvalidNumberOfShips;
import game.exceptions.InvalidShipDimension;
import game.exceptions.NoSuchCoordinateException;
import game.exceptions.PositionAlreadyTaken;
import game.missiles.MissileType;
import game.player.Player;
import game.ship.BattleShip;
import game.ship.BattleShipState;
import game.ship.BattleShipType;

public class Board {

	private int width;
	private XCoordinate height;
	private BattleShip board[][];
	private int noOfShips;

	public Board(int w,String ht,int shipsNum) throws InvalidBoardDimension, InvalidNumberOfShips, NoSuchCoordinateException
	{
		if(w<1 || w>9)
		{
			throw new InvalidBoardDimension("Width must be 1 to 9 and height must be A to Z");
		}
		this.width = w;
		try
		{
			this.height = XCoordinate.valueOf(ht);
		}catch(IllegalArgumentException e)
		{
			throw new NoSuchCoordinateException(ht+" is not a valid coordinate.");
		}

		if(shipsNum<1 || shipsNum> (width*height.getValue()))
		{
			throw new InvalidNumberOfShips("Ships should be between 1 to board widthxheight");
		}

		board = new BattleShip[height.getValue()][width];

	}
	public int getWidth() {
		return width;
	}
	public XCoordinate getHeight() {
		return height;
	}
	public BattleShip[][] getBoard() {
		return board;
	}
	public int getNoOfShips() {
		return noOfShips;
	}

	public void addShipOnBoard(BattleShip ship) throws PositionAlreadyTaken, InvalidShipDimension
	{
		Set<Coordinates> shipCoordinates = ship.getShipCoordinates();
		for(Coordinates coord:shipCoordinates)
		{
			int x = coord.getX();
			int y = coord.getY();
			//System.out.println("x="+x+" y="+y+" coord-"+coord);
			if(y>width || x>height.getValue())
			{
				throw new InvalidShipDimension(x+","+y+" exceeds Board dimension of "+height+","+width);
			}
				
			if(board[x-1][y-1]!=null)
			{
				throw new PositionAlreadyTaken(coord+" is already taken");
			}
			board[x-1][y-1] = ship;
		}
	}

	public BattleShip getShipOnBoard(Coordinates coord)
	{
		int x = coord.getX();
		int y = coord.getY();
		return board[x-1][y-1];
	}


	public boolean targetShip(Coordinates coord, MissileType missileType)
	{
		int x = coord.getX();
		int y = coord.getY();
		BattleShip ship = board[x-1][y-1];
		boolean shipHit = false;
		if(ship!=null)
		{
				shipHit = updateShipOnBoard(ship,coord,missileType);
		}
		return shipHit;
	}

	private boolean updateShipOnBoard(BattleShip ship,Coordinates coord,MissileType missileType)
	{
		boolean shipHit = false;
		BattleShipType shipType = ship.getShipType();
		Set<Coordinates> shipCoordinates = ship.getShipCoordinates();
		int noOfHitCoords = 0;
		int missileHitCount = missileType.getNoOfHits();
		for(Coordinates coord1:shipCoordinates)
		{
			int hits = coord1.getHitCount();
			if(coord.equals(coord1) && (hits+1 <= shipType.getShipType().getMaxHits()))
			{
				hits = coord1.incrementHitCount(missileHitCount);
				shipHit = true;
			}

			if(hits >= shipType.getShipType().getMaxHits())
			{

				noOfHitCoords++;
			}

		}

		if(noOfHitCoords< shipCoordinates.size())
		{
			ship.setState(BattleShipState.HIT);

		}
		else if(noOfHitCoords>=shipCoordinates.size())
		{
			ship.setState(BattleShipState.DESTROYED);
		
			removeShipFromBoard(ship);
		}
		return shipHit;
	}

	private void removeShipFromBoard(BattleShip ship)
	{
		Set<Coordinates> shipCoordinates = ship.getShipCoordinates();
		for(Coordinates coord:shipCoordinates)
		{
			int x = coord.getX();
			int y = coord.getY();
			board[x-1][y-1] = null;
		}
		System.out.println("Ship has been completely destroyed");
		Player p = ship.getPlayer();
		p.removeShip(ship);
	}

}
