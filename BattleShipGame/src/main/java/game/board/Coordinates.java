package game.board;
import java.util.Objects;

import game.exceptions.InvalidCoordinates;

public class Coordinates {

	private XCoordinate xc;
	private int x, y;
	private int hitCount;

	public Coordinates(String xy) throws InvalidCoordinates
	{
		try
		{
			xc = XCoordinate.valueOf(xy.charAt(0)+"");
			y = Integer.parseInt(xy.substring(1));
			x = xc.getValue();
		}
		catch(Exception e)
		{
			throw new InvalidCoordinates("Invalid coordinates-"+e.getMessage());
		}
	}

	public Coordinates(int x,int y) throws InvalidCoordinates
	{
		try
		{
			this.x = x;
			this.xc = XCoordinate.fromOrdinal(x);
			this.y = y;
		}
		catch(Exception e)
		{
			throw new InvalidCoordinates("Invalid coordinates-"+e.getMessage());
		}
	}
	public Coordinates(Coordinates c) throws InvalidCoordinates
	{
		try
		{
			this.x = c.getX();
			this.xc = c.getXc();
			this.y = c.getY();
		}
		catch(Exception e)
		{
			throw new InvalidCoordinates("Invalid coordinates-"+e.getMessage());
		}
	}


	public XCoordinate getXc() {
		return xc;
	}

	public int getX() {
		return x;
	}

	public  int getY() {
		return y;
	}

	public static Coordinates addToX(Coordinates coord,int num) throws InvalidCoordinates
	{
		Coordinates c = new Coordinates(coord.getX()+num,coord.getY());
		return c;
	}

	public static Coordinates addToY(Coordinates coord,int num) throws InvalidCoordinates
	{
		Coordinates c = new Coordinates(coord.getX(),coord.getY()+num);
		return c;
	}

	public int getHitCount() {
		return hitCount;
	}

	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}

	public int incrementHitCount() {
		hitCount = hitCount+1;
		return hitCount;
	}


	public int incrementHitCount(int hits) {
		hitCount = hitCount+hits;
		return hitCount;
	}
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
			return false;
		if(obj instanceof Coordinates)
		{
			Coordinates p = (Coordinates)obj;
			if((this.getX() == p.getX()) && (this.getY() == p.getY()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(this.getX(),this.getY());
	}

	@Override
	public String toString()
	{
		return xc.name()+y;
	}


}
