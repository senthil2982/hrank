package game.board;

public enum XCoordinate {

	A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z;

	private static final XCoordinate[] lookup  = XCoordinate.values();

	public int getValue() {
		return ordinal() + 1;
	}
	public static XCoordinate fromOrdinal(int ordinal)
	{
		return lookup[ordinal-1];
	}
}
