package game.exceptions;

public class ExceededShipCount extends Exception{
	private static final long serialVersionUID = 1L;

	public ExceededShipCount(String string) {
		super(string);
	}

}
