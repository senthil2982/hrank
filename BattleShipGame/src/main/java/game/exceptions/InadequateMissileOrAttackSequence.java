package game.exceptions;

public class InadequateMissileOrAttackSequence extends Exception{
	private static final long serialVersionUID = 1L;

	public InadequateMissileOrAttackSequence(String string) {
		super(string);
	}

}
