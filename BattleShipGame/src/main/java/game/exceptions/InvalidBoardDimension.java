package game.exceptions;

public class InvalidBoardDimension extends Exception{
	private static final long serialVersionUID = 1L;

	public InvalidBoardDimension(String string) {
		super(string);
	}

}
