package game.exceptions;

public class InvalidCoordinates extends Exception{
	private static final long serialVersionUID = 1L;

	public InvalidCoordinates(String string) {
		super(string);
	}

}
