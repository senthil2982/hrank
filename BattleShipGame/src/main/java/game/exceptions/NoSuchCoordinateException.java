package game.exceptions;

public class NoSuchCoordinateException extends Exception{
	private static final long serialVersionUID = 1L;

	public NoSuchCoordinateException(String string) {
		super(string);
	}

}
