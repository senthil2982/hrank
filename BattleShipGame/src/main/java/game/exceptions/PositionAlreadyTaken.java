package game.exceptions;

public class PositionAlreadyTaken extends Exception{
	private static final long serialVersionUID = 1L;

	public PositionAlreadyTaken(String string) {
		super(string);
	}


}
