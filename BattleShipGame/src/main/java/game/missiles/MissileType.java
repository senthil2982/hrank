package game.missiles;

public enum MissileType {
	L(1), M(2);
 
	private int noOfHits;
	public int getNoOfHits() {
		return noOfHits;
	}
	private MissileType(int hits)
	{
		noOfHits = hits;
	}
	
}
