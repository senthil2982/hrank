package game.player;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

import game.board.Board;
import game.board.Coordinates;
import game.exceptions.ExceededShipCount;
import game.exceptions.InadequateMissileOrAttackSequence;
import game.exceptions.InvalidCoordinates;
import game.exceptions.InvalidShipDimension;
import game.exceptions.PositionAlreadyTaken;
import game.missiles.MissileType;
import game.ship.BattleShip;

public class Player{
	
	private String playerName;
	private Board battleBoard;
	private int maxNoOfShips;
	private Queue<Coordinates> attackQueue;
	private Queue<MissileType> missileQueue;
	private Set<BattleShip> ships;

public Player(String name, Board bd, int shipcount)
{
	playerName = name;
	battleBoard = bd;
	maxNoOfShips = shipcount;
	attackQueue = new LinkedList<Coordinates>();
	missileQueue = new LinkedList<MissileType>();
	ships = new HashSet<BattleShip>();
}

public void addShip(BattleShip shp) throws ExceededShipCount, PositionAlreadyTaken, InvalidShipDimension
{
	if(ships.size()+1>maxNoOfShips)
		throw new ExceededShipCount(playerName+" max ship count per player reached");
	if(ships.contains(shp))
		throw new PositionAlreadyTaken("Ship with "+shp.getShipCoordinates()+" is already added");
	ships.add(shp);
	battleBoard.addShipOnBoard(shp);
}

public void removeShip(BattleShip shp)
{
	ships.remove(shp);
}

public void addAttackSequence(String[] arr) throws InvalidCoordinates, InadequateMissileOrAttackSequence
{
	for(String str:arr)
	{
		Coordinates c = new Coordinates(str);
		attackQueue.add(c);
	}
	if(attackQueue.size()!=missileQueue.size())
		throw new InadequateMissileOrAttackSequence(this.getPlayerName()+" attackSequence count "+attackQueue.size()+" missileSequence count "+missileQueue.size()+" didn't match");
}

public void addMissileSequence(String[] arr) throws InvalidCoordinates
{
	for(String str:arr)
	{
		MissileType m = MissileType.valueOf(str);
		missileQueue.add(m);
	}
}

public String getPlayerName() {
	return playerName;
}

public Board getBattleBoard() {
	return battleBoard;
}

public int getMaxNoOfShips() {
	return maxNoOfShips;
}

public Set getShips() {
	return ships;
}

public Coordinates getNextAttackCoordinate()
{
	return attackQueue.poll();
}

public MissileType getNextMissileType()
{
	return missileQueue.poll();
}


public void setBattleBoard(Board battleBoard) {
	this.battleBoard = battleBoard;
}

@Override
public boolean equals(Object obj)
{
	if(obj == null)
		return false;
	if(obj instanceof Player)
	{
		Player p = (Player)obj;
		if(this.getPlayerName().equals(p.getPlayerName()))
			return true;
	}
	return false;
}

@Override
public int hashCode()
{
	return Objects.hash(this.getPlayerName());
}

public boolean targetShip(Coordinates coord, MissileType missileType) {
		return battleBoard.targetShip(coord,missileType);
	
}

}
