package game.player;
import java.util.ArrayList;
import java.util.List;

public class PlayersCircularList {
	
	private List<Player> playerList;
	private int currentIndex;
	private int opponentIndex;
	
	public PlayersCircularList()
	{
		playerList = new ArrayList<Player>();
		currentIndex = 0;
		opponentIndex = 1;
	}
	
	public void add(Player p)
	{
		playerList.add(p);
	}
	
	public void addAll(PlayersCircularList p)
	{
		playerList.addAll(p.getPlayerList());
	}
	
	public void remove(int i)
	{
		playerList.remove(i);
	}
	
	public int getPlayersSize()
	{
		return playerList.size();
	}
	
	public Player getNextPlayer()
	{
		
		Player p = playerList.get(currentIndex);
		currentIndex = (currentIndex+1)%playerList.size();
		return p;
	}
	
	public Player getOpponentPlayer()
	{
		Player p = playerList.get(opponentIndex);
		opponentIndex = (opponentIndex+1)%playerList.size();
		return p;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}
	
	
	public List<Player> getPlayerList() {
		return playerList;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	public void reset()
	{
		this.currentIndex = 0;
		this.opponentIndex = 1;
	}

}
