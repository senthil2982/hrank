package game.ship;
import java.util.Objects;
import java.util.Set;

import game.board.Board;
import game.board.Coordinates;
import game.exceptions.InvalidCoordinates;
import game.exceptions.InvalidShipDimension;
import game.player.Player;

public class BattleShip {

	private Set<Coordinates> shipCoordinates;
	private BattleShipType shipType;
	private BattleShipState state;
	private Player player;

	public BattleShip(Player p, BattleShipType stype, String startingPoint, Board bd) throws InvalidShipDimension, InvalidCoordinates
	{
		this.player = p;
		shipType = stype;
		Coordinates xy = new Coordinates(startingPoint);
		shipCoordinates = stype.getShipsCoordinates(xy);
		state = BattleShipState.UP;
	}

	public Set<Coordinates> getShipCoordinates() {
		return shipCoordinates;
	}

	public BattleShipType getShipType() {
		return shipType;
	}

	public BattleShipState getState() {
		return state;
	}

	public Player getPlayer() {
		return player;
	}

	public void setState(BattleShipState state) {
		this.state = state;
	}

	public void hit()
	{
		if(state.equals(BattleShipState.UP) && shipType.getShipType().getMaxHits()==2)
		{
			state = BattleShipState.HIT;
		}
		else if(state.equals(BattleShipState.HIT) && shipType.getShipType().getMaxHits()==2)
		{
			state = BattleShipState.DESTROYED;
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
			return false;
		if(obj instanceof BattleShip)
		{
			BattleShip p = (BattleShip)obj;
			if(this.getShipType().equals(p.getShipType()))
			{
				if(shipCoordinates.size()!=p.getShipCoordinates().size())
					return false;
				for(Coordinates c:p.getShipCoordinates())
				{
					if(!shipCoordinates.contains(c))
						return false;
				}
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(this.getShipType(),this.getShipCoordinates(),this.getPlayer());
	}

}
