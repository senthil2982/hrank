package game.ship;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import game.board.Board;
import game.board.Coordinates;
import game.exceptions.InvalidCoordinates;
import game.exceptions.InvalidShipDimension;
import game.exceptions.InvalidShipType;

public class BattleShipType {

	private ShipType shipType;
	private int width,height;

	public BattleShipType(String type,int w,int h,Board bd) throws InvalidShipType, InvalidShipDimension
	{
		try
		{
			shipType = ShipType.valueOf(type);
		}
		catch(Exception e)
		{
			throw new InvalidShipType("Invalid Ship type - "+type);
		}
		if(w>bd.getWidth() || h>bd.getHeight().getValue())
			throw new InvalidShipDimension(w+","+h+" exceeds board dimensions"+bd.getWidth()+","+bd.getHeight().getValue());

		width = w;
		height = h;
	}

	public Set<Coordinates> getShipsCoordinates(Coordinates startingPoint) throws InvalidCoordinates
	{
		Set<Coordinates> coordsList = new HashSet<Coordinates>();
		int startx = startingPoint.getX();
		int starty = startingPoint.getY();
		int x=0,y=0;
		for(int i = 0;i<width;i++)
		{
			y = i>0?y + 1:starty;// adding the starting coordinate itself.

			for(int j =0;j<height;j++)
			{
				x = j>0?x + 1:startx; // adding the starting coordinate itself.
				Coordinates coor1 = new Coordinates(x,y);
				coordsList.add(coor1);
				//System.out.println("coor1-"+coor1);
			}
		}
		return coordsList;
	}

	public ShipType getShipType() {
		return shipType;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
			return false;
		if(obj instanceof BattleShipType)
		{
			BattleShipType p = (BattleShipType)obj;
			if(this.getShipType().equals(p.getShipType()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(this.getShipType());
	}



}
