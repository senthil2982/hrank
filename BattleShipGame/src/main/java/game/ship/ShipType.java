package game.ship;

public enum ShipType {
	P(1),Q(2);
	private final int maxHits;
	private ShipType(int hits)
	{
		maxHits = hits;
	}
	public int getMaxHits() {
		return maxHits;
	}
	
}
