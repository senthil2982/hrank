package game;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import org.junit.jupiter.api.Test;

class BattleShipGameTests {

	@Test
	void test() {
		String arr[]= {
				"Player-1 fires a missile with target A1 which got miss",
				"Player-2 fires a missile with target A1 which got hit",
				"Player-2 fires a missile with target B2 which got miss",
				"Player-1 fires a missile with target B2 which got hit",
				"Player-1 fires a missile with target B2 which got hit",
				"Player-1 fires a missile with target B3 which got miss",
				"Player-2 fires a missile with target B3 which got miss",
				"Player-1 has no more missiles left to launch",
				"Player-2 fires a missile with target A1 which got hit",
				"Player-2 fires a missile with target D1 which got miss",
				"Player-1 has no more missiles left to launch",
				"Player-2 fires a missile with target E1 which got miss",
				"Player-1 has no more missiles left to launch",
				"Player-2 fires a missile with target D4 which got hit",
				"Player-2 fires a missile with target D4 which got miss",
				"Player-1 has no more missiles left to launch",
				"Player-2 fires a missile with target D5 which got hit",
				"Player-2 won the battle"
		};

		try
		{

			java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();

			System.setOut(new java.io.PrintStream(out));
			BattleShipGame bsg = new BattleShipGame();
			bsg.playGame(new File("battleshipinput.txt"));
			String outputStr = out.toString().trim();
			//System.err.println("Out was: " + outputStr);
			String str = String.join("\n", arr).trim();
			//System.err.println("in was: " + str);
			if(!outputStr.equals(str))
			{
				fail(outputStr+" didn't match "+str);
			}



		}catch(Exception e)
		{
			e.printStackTrace();
			fail(e.getMessage());

		}

	}

}
