package game;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import game.board.Board;
import game.exceptions.ExceededShipCount;
import game.exceptions.InvalidBoardDimension;
import game.exceptions.InvalidNumberOfShips;
import game.exceptions.InvalidShipDimension;
import game.exceptions.NoSuchCoordinateException;
import game.exceptions.PositionAlreadyTaken;
import game.player.Player;
import game.ship.BattleShip;
import game.ship.BattleShipType;

class ExceptionsTest {

	 @Test
	public void testInvalidBoardDimension()
	{
		 assertThrows(InvalidBoardDimension.class, () -> {
		        Board b = new Board(0,"Z",2);
		    });
		 assertThrows(InvalidBoardDimension.class, () -> {
		        Board b = new Board(10,"Z",2);
		    });
 
	}
	 @Test
	 public void testNoSuchCoordinateException()
	 {
		 assertThrows(NoSuchCoordinateException.class, () -> {
		        Board b = new Board(1,"AZ",2);
		    });
		 assertThrows(NoSuchCoordinateException.class, () -> {
		        Board b = new Board(2,"2A",2);
		    });
	 }
	 
	 @Test
	 public void testInvalidNumberOfShips()
	 {
		 assertThrows(InvalidNumberOfShips.class, () -> {
		        Board b = new Board(4,"D",20000);
		    });
	 }
	 
	
	 @Test
	 public void testInvalidShipDimension()
	 {
		 assertThrows(InvalidShipDimension.class, () -> {
		        Board b = new Board(4,"D",2);
		        int sw = 2;
				int sh = 20;
				BattleShipType shipType1 = new BattleShipType("P",sw,sh,b);
		    });
	 }
	 
	 @Test
	 public void testAddingShips()
	 {
			assertThrows(PositionAlreadyTaken.class, () -> {
			 	int width = 4;
			 	String height = "D";
			 	int noOfShips = 4;
				Board board1 = new Board(width,height,noOfShips);
				Player p = new Player("Player-1",board1,noOfShips);
				int sw = 2;
				int sh = 2;
				BattleShipType shipType1 = new BattleShipType("P",sw,sh,board1);
				BattleShip shp = new BattleShip(p, shipType1,"C2",p.getBattleBoard());
				BattleShip shp1 = new BattleShip(p, shipType1,"C2",p.getBattleBoard());
				p.addShip(shp);
				p.addShip(shp1);
			});
			
			assertThrows(InvalidShipDimension.class, () -> {
			 	int width = 4;
			 	String height = "D";
			 	int noOfShips = 2;
				Board board1 = new Board(width,height,noOfShips);
				Player p = new Player("Player-1",board1,noOfShips);
				int sw = 2;
				int sh = 2;
				BattleShipType shipType1 = new BattleShipType("P",sw,sh,board1);
				BattleShip shp = new BattleShip(p, shipType1,"D2",p.getBattleBoard());
				BattleShip shp1 = new BattleShip(p, shipType1,"A3",p.getBattleBoard());
				p.addShip(shp);
				p.addShip(shp1);
			});
			
			assertThrows(ExceededShipCount.class, () -> {
			 	int width = 4;
			 	String height = "E";
			 	int noOfShips = 2;
				Board board1 = new Board(width,height,noOfShips);
				Player p = new Player("Player-1",board1,noOfShips);
				int sw = 2;
				int sh = 2;
				BattleShipType shipType1 = new BattleShipType("P",sw,sh,board1);
				BattleShip shp = new BattleShip(p, shipType1,"D2",p.getBattleBoard());
				BattleShip shp1 = new BattleShip(p, shipType1,"A3",p.getBattleBoard());
				BattleShip shp2 = new BattleShip(p, shipType1,"C1",p.getBattleBoard());
				p.addShip(shp);
				p.addShip(shp1);
				p.addShip(shp2);
			});
			
	 }
}
