/*
 * BinaryTreeApp.cpp
 *
 *  Created on: Jun 24, 2016
 *      Author: ssundhar
 */
#include <stdio.h>
#include <stdlib.h>

struct Node
{
	int key;
	Node *right, *left;
};

Node* newNode(int data)
{
	Node *n = new Node;
	n->key = data;
	n->right = n->left = NULL;
	return n;
}

void inorder(Node *root)
{
	if (root != NULL)
	{
		inorder(root->left);
		printf("%d\t", root->key);
		inorder(root->right);
	}
}

Node* insert(Node *root, int key)
{
	if (root == NULL) return newNode(key);
	if (key < root->key)
	{
		root->left = insert(root->left, key);
	}
	else if (key > root->key)
	{
		root->right = insert(root->right, key);
	}

	return root;
}

Node* minValue(Node *node)
{
	Node *current = node;
	while (current->left!= NULL)
	{
		current = current->left;
	}
	return current;
}

Node* deleteNode(Node *root, int key)
{
	// base case
	if (root == NULL) return root;
	/*Keep navigating till the key = root.key*/
	if (key < root->key)
	{
		root->left = deleteNode(root->left, key);
	}
	else if (key > root->key)
	{
		root->right = deleteNode(root->right, key);
	}
	else
	{
		/*Now key = root.key

		*/
		if (root->left == NULL)
		{
			Node *temp = root->right;
			free(root);
			return temp;
		}
		else if (root->right == NULL)
		{
			Node *temp = root->left;
			free(root);
			return temp;
		}
		// find min value in right sub tree
		Node *temp = minValue(root->right);
		//copy the minvalue of the right subtree to root.
		root->key = temp->key;
		//delete the minvalue node
		root->right = deleteNode(root->right, temp->key);
	}
	return root;
}
int count = 0;
void printReverseInorder(Node *node, int k)
{
	if (node != NULL && count <= k)
	{
		printReverseInorder(node->left, k);

		count++;
		if (count == k)
		{
			printf("%d\t", node->key);
		}
		printReverseInorder(node->right, k);

	}
}

void findElement(Node *root, int k)
{
	if (root != NULL)
	{
		if (k == root->key)
		{
			printf("\nElement found - %d", root->key);
		}
		else if (k > root->key)
			findElement(root->right, k);
		else if(k< root->key)
			findElement(root->left, k);

	}
	else
	{
		printf("\nElement not found");
	}
}

int main(void)
{
	/* Let us create following BST
	50
	/     \
	30      70
	/  \    /  \
	20   40  60   80 */
	Node *root = NULL;
	root = insert(root, 50);
	insert(root, 30);
	insert(root, 40);
	insert(root, 20);
	insert(root, 60);
	insert(root, 70);
	insert(root, 80);
	printf("Inorder traversal of the given tree \n");
	inorder(root);
	printf("\nReverse Inorder traversal of the given tree \n");
	printReverseInorder(root, 2);
	findElement(root, 20);
	printf("\nDelete 20\n");
	root = deleteNode(root, 20);
	printf("Inorder traversal of the modified tree \n");
	inorder(root);
	findElement(root, 20);
	printf("\nDelete 30\n");
	root = deleteNode(root, 30);
	printf("Inorder traversal of the modified tree \n");
	inorder(root);

	printf("\nDelete 50\n");
	root = deleteNode(root, 50);
	printf("Inorder traversal of the modified tree \n");
	inorder(root);

	getchar();
	return 0;
}


