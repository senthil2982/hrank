
public class EvenOddObject {

	private volatile boolean evenPrinted;

	public boolean isEvenPrinted() {
		return evenPrinted;
	}

	public synchronized void setEvenPrinted(boolean evenPrinted) {
		this.evenPrinted = evenPrinted;
	}
	
}
