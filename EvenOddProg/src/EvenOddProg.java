
public class EvenOddProg {

	public static void main(String args[])
	{
		EvenOddObject evenOdd = new EvenOddObject();
		EvenThread evenT = new EvenThread(evenOdd,0,10);
		OddThread oddT = new OddThread(evenOdd,1,10);
		Thread t1 = new Thread(evenT);
		Thread t2 = new Thread(oddT);
		t1.start();
		t2.start();
	}
	
	
}
