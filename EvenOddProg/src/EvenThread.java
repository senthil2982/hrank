
public class EvenThread implements Runnable {
	EvenOddObject evenOdd;
	int end;
	int value;
	public EvenThread(EvenOddObject obj, int start,int last)
	{
		evenOdd = obj;
		value = start;
		end = last;
	}
	
	public void run()
	{
		while(value<=end)
		{
			if(!evenOdd.isEvenPrinted())
			{
				System.out.println(value);
				value+=2;
				evenOdd.setEvenPrinted(true);
				synchronized(evenOdd)
				{
				evenOdd.notify();
				}
			}
			else
			{
				while(evenOdd.isEvenPrinted())
				{
					try
					{
						synchronized(evenOdd)
						{
							System.out.println("Even waiting");
							evenOdd.wait();
							System.out.println("Even not waiting");
						}
					}catch(Exception e){e.printStackTrace();}
				}
			}
		}
	}
}
