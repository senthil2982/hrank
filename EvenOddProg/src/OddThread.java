
public class OddThread implements Runnable{
	
	EvenOddObject evenOdd;
	int end;
	int value;
	public OddThread(EvenOddObject obj,  int start,int last)
	{
		evenOdd = obj;
		value = start;
		end = last;
	}
	
	public void run()
	{
		while(value<=end)
		{
			if(evenOdd.isEvenPrinted())
			{
				System.out.println(value);
				value+=2;
				evenOdd.setEvenPrinted(false);
				synchronized(evenOdd)
				{
				evenOdd.notify();
				}
			}
			else
			{
				while(!evenOdd.isEvenPrinted())
				{
					try
					{
						synchronized(evenOdd)
						{
							System.out.println("Odd waiting");
							evenOdd.wait();
							System.out.println("Odd not waiting");
						}
					}catch(Exception e){e.printStackTrace();}
				}
			}
		}
	}

}
