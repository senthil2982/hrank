package com.thalum.server.jetty;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ServerConfig {
	private static Properties serverprops = new Properties();

	static
	{
		try {
			InputStream f = ServerConfig.class.getResourceAsStream("/server.properties");
			serverprops.load(f);
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static Properties getServerprops() {
		return serverprops;
	}

	public static void setServerprops(Properties serverprops) {
		ServerConfig.serverprops = serverprops;
	}
	
	public static String getProperty(String key)
	{
		return serverprops.getProperty(key);
	}
}
