package com.thalum.server.jetty;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

import com.thalum.server.servlet.FileCollectionServlet;
import com.thalum.server.jetty.ServerConfig;


public class ServerStartUp {

	public static void main(String[] args) throws Exception {
		ServerStartUp sUp = new ServerStartUp();
		sUp.startServer();

	}

	public void startServer() throws Exception
	{
	   // Log.getRootLogger().setDebugEnabled(true);
		Server server = new Server(Integer.parseInt(ServerConfig.getProperty("HOST_PORT")));

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

//        context.setContextPath(ServerConfig.getProperty("CONTEXT_PATH"));
        context.setResourceBase("src/main/webapp");
        server.setHandler(context);

        // Add dump servlet
        context.addServlet(FileCollectionServlet.class, "/filecollectionservlet");
        // Add default servlet
        context.addServlet(DefaultServlet.class, "/");

        server.start();
        server.join();
	}
}
