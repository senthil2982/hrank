import java.util.Date;

public abstract class BasicMetric implements Metric {
	String appName;
	String metricName;
	Date creationTime;
	public BasicMetric(String appName, String metricName, Date creationTime) {
		super();
		this.appName = appName;
		this.metricName = metricName;
		this.creationTime = creationTime;
	}
}
