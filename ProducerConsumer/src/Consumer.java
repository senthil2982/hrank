import java.util.Queue;

public class Consumer implements Runnable {
	private Queue<Metric> queue;

	public Consumer(Queue<Metric> queue) {
		this.queue = queue;

	}

	@Override
	public void run() {
		while (true) {
			synchronized (queue) {
				while (queue.isEmpty()) {
					System.out.println("Queue is empty, so waiting -"+Thread.currentThread().getName());
					try {
						queue.wait();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
				Metric m = queue.remove();
				System.out.println(Thread.currentThread().getName()+ " Sending to server : " + m.toString());
				synchronized (queue) {
				queue.notify();
				}
				//sleep added to slow the process
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
		}
	}

}
