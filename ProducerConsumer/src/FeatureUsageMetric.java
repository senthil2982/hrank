import java.util.Date;

public class FeatureUsageMetric extends BasicMetric {
	
	String deviceInfo;
	int noOfClicks;
	public FeatureUsageMetric(String appName, String metricName, Date creationTime, String deviceInfo, int noOfClicks )
	{
		super( appName,  metricName, creationTime);
		this.deviceInfo = deviceInfo;
		this.noOfClicks = noOfClicks;
	}
	
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append("Metric info - appName-");
		str.append(appName);
		str.append(" metricName:");
		str.append(metricName);
		str.append(" creationTime ");
		str.append(creationTime);
		return str.toString();
	}

}
