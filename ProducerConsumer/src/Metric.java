import java.util.Date;

public interface Metric {
	public enum priority { URGENT, HIGH, NORMAL };
	public enum metricType { FEATUREUSAGE, STABILITY, PERFORMANCE };
}
