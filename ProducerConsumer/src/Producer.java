import java.util.Date;
import java.util.Queue;

class Producer implements Runnable {
	private Queue<Metric> queue;
	private int maxSize;

	public Producer(Queue<Metric> queue, int maxSize) {
		this.queue = queue;
		this.maxSize = maxSize;
	}

	@Override
	public void run() {
		while(true)
		{
			synchronized (queue) {
				while (queue.size() == maxSize) {
					try {
						System.out.println("Queue is full, so waiting "+Thread.currentThread().getName());
						queue.wait();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
				Metric m = createMetric();
				queue.add(m);
				System.out.println(Thread.currentThread().getName()+" Metric added to queue: "+m.toString());
				synchronized (queue) {
				queue.notify();
				}
				//sleep added to slow the process
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
		}
	}
	private Metric createMetric()
	{
		Metric metric = new FeatureUsageMetric("TestApp1","FeatureMetric1",new Date(System.currentTimeMillis()),"Moto g4",3);
		return metric;
	}
	
	}
