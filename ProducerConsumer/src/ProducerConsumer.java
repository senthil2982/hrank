import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ProducerConsumer {
	
	private Queue<Metric> sharedQueue;
	private int queueMaxSize;
	
	public ProducerConsumer(int queueSize)
	{
		this.sharedQueue =  new ConcurrentLinkedQueue<Metric>();
		this.queueMaxSize = queueSize;
	}

	public static void main(String argv[])
	{
		ProducerConsumer pc = new ProducerConsumer(10);
		Producer producer = new Producer(pc.getSharedQueue(),pc.getQueueMaxSize());
		Consumer consumer = new Consumer(pc.getSharedQueue());
		ThreadPool producerpool = new ThreadPool("producer", 5, producer);
		ThreadPool consumerpool = new ThreadPool("consumer", 3, consumer);
		producerpool.createAndStartThreads();
		consumerpool.createAndStartThreads();
	}

	public Queue<Metric> getSharedQueue() {
		return sharedQueue;
	}

	public void setSharedQueue(Queue<Metric> sharedQueue) {
		this.sharedQueue = sharedQueue;
	}

	public int getQueueMaxSize() {
		return queueMaxSize;
	}

	public void setQueueMaxSize(int queueMaxSize) {
		this.queueMaxSize = queueMaxSize;
	}
}
