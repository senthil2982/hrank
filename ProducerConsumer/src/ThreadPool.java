import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ThreadPool {

	int noOfThreads;
	List<Thread> workerThreads;
	String poolName;
	Runnable workerTask;
	
	public ThreadPool(String poolName, int noOfThreads, Runnable worker)
	{
		this.poolName = poolName;
		if(noOfThreads>0)
		this.noOfThreads = noOfThreads;
		this.workerTask = worker;
		this.workerThreads = new LinkedList<Thread>();

	}
	
	public void createAndStartThreads()
	{
		for(int i=0;i<noOfThreads;i++)
		{
			Thread t = new Thread(workerTask,poolName+"-worker-"+i);
			t.start();
			System.out.println("Thread - "+t.getName()+" started");
			workerThreads.add(t);
			
		}
	}
	
	

}
