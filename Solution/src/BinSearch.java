import java.util.Scanner;


public class BinSearch {
	public static class Node {
		public int data;
		public Node right;
		public Node left;
		
		public Node(int val)
		{
			data = val;
			left = right = null;
		}
	}
	
	public static void insertTree(Node n, int val)
	{
		if(val <= n.data)
		{
			if(n.left == null)
				n.left = new Node(val);
			else
				insertTree(n.left,val);
		}
		else if(val > n.data)
		{
			if(n.right == null)
				n.right = new Node(val);
			else
				insertTree(n.right,val);
		}
	}
	
	public static void printInOrder(Node n)
	{
		if(n == null) return;
		printInOrder(n.left);
		System.out.print(" "+n.data);
		printInOrder(n.right);
	}
	
	public static void main(String[] args) {
		 Scanner scanner = new Scanner(System.in);
		 	int numOfElements = scanner.nextInt();
	        Node root = new Node(scanner.nextInt());
	        for(int i=1;i<numOfElements;i++)
	        {
	            int value = scanner.nextInt();
	            insertTree(root,value);
	        }
	        scanner.close();

	       printInOrder(root);

	}

}
