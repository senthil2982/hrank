import java.util.Scanner;
import java.util.Stack;

public class BraceCheck {

	    public static boolean isBalanced(String expression) {
	        Stack s = new Stack();
	        char[] arr = expression.toCharArray();
	        for(int i=0;i<arr.length;i++)
	        {
	        	if(arr[i] == '{' || arr[i]=='(' || arr[i] == '[')
	        	{
	        		s.push(arr[i]);
	        	}
	        	else if(arr[i] == '}' || arr[i]==')' || arr[i] == ']')
	        	{
	        		if(!s.empty())
	        		{
	        		char prev = (char) s.pop();
	        		//System.out.println("comparing "+prev+" "+arr[i]);
	        		if(!checkMatching(arr[i], prev))
	        		{
	        			return false;
	        		}
	        		}
	        		else
	        			return false;
	        	}
	        		
	        }
	        if(s.isEmpty())
	        	return true;
	        return false;
	    }
	    
	    static boolean checkMatching(char a,char b)
	    {
	    	if (((a == '{' && b == '}') || (b == '{' && a == '}')) 
	    	  || ((a == '(' && b == ')') || (b == '(' && a == ')')) 
	    	  || ((a == '[' && b == ']') || (b == '[' && a == ']'))) 
	    		return true;
	    	else
	    		return false;
	    }
	  
	    public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int t = in.nextInt();
	        for (int a0 = 0; a0 < t; a0++) {
	            String expression = in.next();
	            System.out.println( (isBalanced(expression)) ? "YES" : "NO" );
	        }
	    }
	}


