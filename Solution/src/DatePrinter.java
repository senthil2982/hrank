import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DatePrinter {

	static Map monthMap = null;
	static String[] monthArr = {"jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"};
	
	static String getMonth(String month)
	{
		if(monthMap == null)
		{
			monthMap = new HashMap();
			for(int i=0;i<12;i++)
			{
				monthMap.put(monthArr[i],new Integer(i+1).toString());
			}
		}
		return (String) monthMap.get(month);
	}
	
	static void printFormattedDate(String dateIn)
	{
		String dateIn1 = dateIn.trim();
		String datesplit[]= dateIn1.split("\\s+");
		String dayIn = datesplit[0].trim();
		String day = null;
		if(dayIn.indexOf("th")!=-1)
		{
			day = dayIn.substring(0,dayIn.indexOf("th")); 
		}
		else if(dayIn.indexOf("nd")!=-1)
		{
			day = dayIn.substring(0,dayIn.indexOf("nd")); 
		}
		else if(dayIn.indexOf("rd")!=-1)
		{
			day = dayIn.substring(0,dayIn.indexOf("rd")); 
		}
		else if(dayIn.indexOf("st")!=-1)
		{
			day = dayIn.substring(0,dayIn.indexOf("st")); 
		}
		
		String year = datesplit[2].trim();
		String month = datesplit[1].trim().toLowerCase();
		String monthGot = getMonth(month);
		System.out.println(year+"-"+monthGot+"-"+day);
	}

	public static void main(String[] args) {
		//Scanner in = new Scanner(System.in);
		//String dateIn = in.next();
		
		String dateIn = "  24th  Dec   1900     ";
		printFormattedDate(dateIn);

	}

}
