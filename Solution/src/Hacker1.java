import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hacker1 {

	 static long teamFormation(int[] score, int team, int m) {
		 List<Integer> scoreList = new ArrayList();
		 for(int i = 0 ;i<score.length;i++)
		 {
			 scoreList.add(score[i]);
		 }
		 long sum = 0;
		 for(int i=0;i<team;i++)
		 {
			 List newArr = getArray(scoreList,m);
			 int max = 0;int lastidx = 0;
			 for(int j=0;j<newArr.size();j++)
			 {
				 if(max == 0)
				    max = (int) newArr.get(j);
				 else if((Integer)newArr.get(j)>max)
				 {
					 max = (int) newArr.get(j);
					 lastidx = j;
				 }
			 }
			 scoreList.remove(lastidx);
			 sum+=max;
		 }
		 
		return sum;

	    }
	 
	 static List getArray(List score,int m)
	 {
		 List arr = new ArrayList();
		 if(score.size() >= 2*m)
		 {
			 int k = 0;
			 for(int i=0;i<m;i++)
			 {
				 arr.add((int)score.get(i));
			 }
			 for(int j=m;j>0;j--)
			 {
				 arr.add((int)score.get(j));
			 }
			 return arr;
		 }
		 return score;
	 }
	 
	
	public static void main(String[] args) {
		int arr[]={ 17,12,10,2,7,2,11,20,8};
		int team = 3;
		int n = 4;
		System.out.println(teamFormation(arr,team,n));
	        

	    }

	

}
