import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Hacker2 {

	static class Bundle implements Comparable
	{
		public int bundleQuantity;
		public int bundleCost;
		public float pricePerNote;
		public Bundle(int bq,int bc)
		{
			bundleQuantity = bq;
			bundleCost = bc;
			pricePerNote = (float)bq/bc;
		}
		@Override
		public int compareTo(Object o) {
			Bundle that = (Bundle)o;
			final int BEFORE = -1;
		    final int EQUAL = 0;
		    final int AFTER = 1;

		    if ( this.pricePerNote == that.pricePerNote ) return EQUAL;
		    if (this.pricePerNote < that.pricePerNote) return BEFORE;
		    if (this.pricePerNote > that.pricePerNote) return AFTER;
		    return 0;
		}
	
	}
	
	static int budgetShopping(int n, int[] bundleQuantities, int[] bundleCosts) {
		int totalnotes = 0;
		Map<Float,Bundle> bcbq = getMap(bundleQuantities,bundleCosts);
	
		for(Iterator it = bcbq.keySet().iterator();it.hasNext();)
		{
			float pricePerNote = (float)it.next();
			Bundle bdl = (Bundle) bcbq.get(pricePerNote);
			int avlnotes = bdl.bundleQuantity;
			System.out.println(avlnotes+" cost-"+bdl.bundleCost);			
			
			if( n >= bdl.bundleCost)
			{
				totalnotes+= (n/bdl.bundleCost)*avlnotes;	
				n = n % bdl.bundleCost;
				System.out.println(totalnotes+" "+n);	
						
			}
		}
		return totalnotes;
    }
	
	static List sortArr(int[] bq, int[] bc)
	{
		List<Float> v = new ArrayList<Float>();
		for(int i=0;i<bc.length;i++)
		{
			float pricePerNote = (float)bc[i]/bq[i];
			System.out.println(pricePerNote+" "+bc[i]+" "+bq[i]+" "+(bq[i]*pricePerNote));
			v.add(pricePerNote);
		}
		return v;
	}
	
	static Map getMap(int[] bq,int[] bc)
	{
		Map<Float, Bundle> bcbq = new TreeMap();
		for(int i=0;i<bq.length;i++)
		{
			Bundle bdl = new Bundle(bq[i],bc[i]);
			bcbq.put(bdl.pricePerNote,bdl);
		}
		return bcbq;
	}
	
	
	
	public static void main(String[] args) {
		int bq[]={10};
		int bc[] = {2} ;
		int n = 4;
		System.out.println("purchase="+budgetShopping(n,bq,bc));
	        

	    }


}
