import java.util.Scanner;

public class StringAnagram {

	 public static int numberNeeded(String first, String second) {
		  int numNeeded =0;
	      int numArr[] = new int[26];
	      
	      char arr1[] = first.toCharArray();
	      char arr2[] = second.toCharArray();

	      for(int i=0;i<arr1.length;i++)
	      {
	    	  int idx = arr1[i] - 'a';
	    	  numArr[idx]++;
	      }
	      
	      for(int i=0;i<arr2.length;i++)
	      {
	    	  int idx = arr2[i] - 'a';
	    	   numArr[idx]--;
	      }
	      for(int i=0;i<26;i++)
	      {
	    	 		 numNeeded+=Math.abs(numArr[i]);
	      }
	      return numNeeded;
	    }
	  
	    public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        String a = in.next();
	        String b = in.next();
	        System.out.println(numberNeeded(a, b));
	    }
}
