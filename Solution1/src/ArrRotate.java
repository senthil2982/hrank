
public class ArrRotate {
	
	
	
	 public static int[] arrayLeftRotation(int[] a, int n, int k) {
	        
		 int temp[] = new int[n];
		 for(int i=0;i<n;i++)
		 {
			int newPos = (i+k)%n;
			temp[i] = a[newPos];
		 }
		 return temp;
	  }
	    
	
	
	public static void main(String[] args) {
		/*Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
      */
		int[] a = {1,2,3,4,5};
		int n = 5,k=4;
        int[] output = new int[n];
        output = arrayLeftRotation(a, n, k);
        for(int i = 0; i < n; i++)
            System.out.print(output[i] + " ");
            
      
        System.out.println();
	}

}
