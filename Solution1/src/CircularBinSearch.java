
public class CircularBinSearch {
	
	public static int pivotedBinarySearch(int arr[],int start,int end,int key)
	{
		if(start>end) return -1;
		int mid = (start + end)/2;
		if(key == arr[mid]) return mid;
		if(key == arr[start]) return start;
		if(key == arr[end]) return end;
		//check if firsthalf is sorted
		if(arr[start] < arr[mid])
		{
			if(arr[start]<key && arr[mid]>key)
			{
				return pivotedBinarySearch(arr,start,mid,key);
			}
			else
			{
				return pivotedBinarySearch(arr,mid,end,key);
			}
		}
		else
		{
			if(arr[mid]<key && arr[end]>key)
			{
				return pivotedBinarySearch(arr,mid,end,key);
			}
			else
			{
				return pivotedBinarySearch(arr,start,mid,key);
			}
		}
	}
	
	public static int linearSearch(int arr[],int start,int end,int key)
	{
		for(int i=start;i<end;i++)
		{
			if(arr[i]==key)
				return i;
		}
		return -1;
	}

	public static void main(String[] args) {
		 int arr1[] = { 4,5,6,7, 8, 9, 10, 1, 2, 3};
	       int n = arr1.length-1;
	       int key = 5;
	       System.out.println("Index of the element is : "
	                      + pivotedBinarySearch(arr1, 0,n, key));

	}

}
