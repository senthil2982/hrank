import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class Top10FileList {

	Integer size(String path) throws NotAFileException;
	List<String> list(String path) throws NotADirectoryException;
	boolean isDirectory(String path);

	Queue<FileEntry> top10queue = new PriorityQueue<FileEntry>();
	  public List<String> top10AsPerSize(String path){
	    
	    if(path!=null)
	    {
	      if(isDirectory(path))
	      {
	          List<String> filesList = list(path);
	          for(String file:filesList)
	          {
	              if(!isDirectory(file))
	              {
	                addSize(file);
	              }
	              else
	              {
	                  top10AsPerSize(file);
	              }
	          }
	          
	      }
	      else
	      {
	          addSize(path);
	      }
	    }
	      
	      
	  }
	  
	  public static class FileEntry implements Comparable
	   {
	      String path;
	      int size;
	      public FileEntry(String p, int s)
	      {
	          path = p;
	          size =s;
	      }
	      @Override
	      public int compareTo(Object o)
	      {
	          FileEntry ofile = (FileEntry)o;
	          if(this.size>ofile.getSize()) return 1;
	          if(this.size==ofile.getSize()) return 0;
	          
			return -1;
	      }
	      
	      public int getSize()
	      {
	          return size;
	      }
	      public String getPath()
	      {
	          return path;
	      }
	      
	  }
	  
	  
	  public void addSize(String path)
	  {
	      int size = size(path);
	      top10queue.add(new FileEntry(path,size));
	  }
	  
	  public void printTop10()
	  {
	      for(int i=0;i<10;i++)
	      {
	          FileEntry f = top10queue.poll();
	          System.out.println(f.getPath()+" "+f.getSize());
	      }
	  }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
	