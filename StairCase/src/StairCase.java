import java.util.Scanner;

public class StairCase {

	static void printStairCase(int n) {

		for (int j = n; j > 0; j--) {
			int k = j - 1;
			for (int i = 0; i < n; i++) {
				if (k > 0) {
					System.out.print(" ");
					k--;
				} else
					System.out.print("#");

			}
			System.out.print("\n");
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		printStairCase(n);
	}

}
