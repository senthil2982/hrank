
public class EnumTest {

	public enum Stats
	{
		BMC_REGION("vi-home-site");
		
		private final String name;
		Stats(String v)
		{
			name = v;
		}
		@Override
		public String toString()
		{
			return name;
		}
	}
	
	public static void print(String v)
	{
		System.out.println(v);
	}
	
	public static void main(String[] args) {
		//String s = Stats.BMC_REGION;
		EnumTest.print(Stats.BMC_REGION+"_goto");

	}

}
