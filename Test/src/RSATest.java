import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;

public class RSATest {
	
	
	
	static void readCertificate()
	{
		InputStream in = null;
	    try 
	    {
	        in = new BufferedInputStream(System.in);
	        CertificateFactory cf = CertificateFactory.getInstance("X.509");
	        X509Certificate cert = (X509Certificate)cf.generateCertificate(in);
	        System.out.println(cert.toString());
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}
	
	static void readCertificate1()
	{
		StringBuilder sb = new StringBuilder();
		BufferedReader in = null;
	    try 
	    { 
	    	 in = new BufferedReader(new InputStreamReader(System.in));
	    	 String str;
	    	 if((str = in.readLine()).contains("BEGIN CERTIFICATE"))
	    	 {
		    	 sb.append(str+"\n");
		    	 while(!(str = in.readLine()).contains("END CERTIFICATE"))
		    	 {
			    	sb.append(str+"\n");
		    	 }
		    	 sb.append(str);
	    	 }
	        System.out.println(sb.toString());
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}

	static void readRSAFromFile()
	{
		Path path = Paths.get("/home/ssundhar/work/workspace/Test/src/rsa.txt");
		try
		{
		byte[] privKeyByteArray = Files.readAllBytes(path);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privKeyByteArray);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		RSAPrivateKey myPrivKey =(RSAPrivateKey)keyFactory.generatePrivate(keySpec);
		System.out.println("Algorithm: " + myPrivKey.getAlgorithm());
		}
		 catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}
	
	static void readRSA()
	{
		InputStream in = null;
	    try 
	    {
	    	 in = new BufferedInputStream(System.in);
   	    	ByteArrayOutputStream out = new ByteArrayOutputStream();
	    	byte[] buffer = new byte[1024];
	    	/*int r;
	    	while ((r = in.read(buffer))!=-1)
	    	{
	    	    out.write(buffer, 0, r);
	    	}*/
	    	int bytesRead;
	    	while ((bytesRead = System.in.read(buffer)) > 0) {
	    	    out.write(buffer, 0, bytesRead);
	    	}
	    	in.close();
	    	out.flush();
	    	byte[] privKeyBytes = out.toByteArray();
	    	KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            KeySpec ks = new PKCS8EncodedKeySpec(privKeyBytes);
            RSAPrivateKey privKey = (RSAPrivateKey) keyFactory.generatePrivate(ks);
            System.out.println(privKey.toString());
	    }  catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		
	}
	public static void main(String[] args) {
		readCertificate1();
		//readRSAFromFile();
		
		
	}

}
