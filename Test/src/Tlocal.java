import java.util.concurrent.atomic.AtomicInteger;

public class Tlocal implements Runnable{

	public static class ThreadId{
		
		private static AtomicInteger userid = new AtomicInteger(0);
		private static final ThreadLocal tid = new ThreadLocal(){
			@Override
			protected Integer initialValue()
			{
				return userid.get();
			}
		
		};
	
		public static int getId()
		{
			return (int) tid.get();
		}
		
		public static void setId(int val)
		{
			 tid.set(val);
		}
		
		
		
	}
	
	public void run()
	{
		int val = 0;
		while(val <=15)
		{
		val	 =  Tlocal.ThreadId.getId();
		System.out.println(Thread.currentThread().getId()+" - "+val);
		++val;
		Tlocal.ThreadId.setId(val);

		}
	}
	
	public static void main(String[] args) {
		Tlocal tl = new Tlocal();
		for(int i=0;i<4;i++)
		{
			Thread th = new Thread(tl);
			th.start();
		}

	}

}
