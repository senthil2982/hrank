
public class BstAllOps {

	public static class Node
	{
		int data;
		Node left,right;
		public Node(int d)
		{
			data = d;
			left = right = null;
			
		}
	}
	
	Node createBSTFromArr(int arr[],int start,int end)
	{
		if(start > end)
		{
			return null;
		}
		int mid = (start+end)/2;
		Node root = new Node(arr[mid]);
		root.left = createBSTFromArr(arr,start,mid-1);
		root.right = createBSTFromArr(arr,mid+1,end);
		return root;
	}
	
	int findHeight1(Node root)
	{
		if(root == null)
			return 0;
		return Math.max(findHeight1(root.left)+1,findHeight1(root.right)+1);
	}
	
	int findHeight(Node root)
	{
		if(root == null)
			return 0;
		int ldepth = findHeight(root.left);
		int rdepth = findHeight(root.right);
		if(ldepth>rdepth)
			return ldepth+1;
		else
			return rdepth+1;
	}
	
	Node prev = null;
	boolean	checkBST(Node root)
	{
		if(root == null)
			return true;
			if(!checkBST(root.left))
				return false;
			if(prev!=null && prev.data>root.data)
				return false;
			return checkBST(root.right);
		
	}
	
	boolean isBST(Node root, Node left, Node right)
	{
		if(root == null)
			return true;
		if(left!=null && root.data < left.data)
			return false;
		if(right!=null && root.data > right.data)
			return false;
		return isBST(root.left,left,root) && isBST(root.right,root,right);
	}
	
}
