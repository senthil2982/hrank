import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.TreeMap;

public class sortBasedOnOccurences {

	 public static void sortBasedOnOccurences(int arr[])
	 {
	   Map<Integer,Integer> val_Occurences = new HashMap();
	   for(int i=0;i<arr.length;i++)
	   {
	     Integer noOfTimes = val_Occurences.get(arr[i]);
	     if(noOfTimes==null)
	     {
	       val_Occurences.put(arr[i],1);
	     }
	     else
	     {
	       val_Occurences.put(arr[i],noOfTimes+1);
	     }
	   }
	   Map<Integer,Integer> sortedMap = new TreeMap();
	   for(Iterator iter = val_Occurences.keySet().iterator();iter.hasNext();)
	   {
	     Integer value = (Integer)iter.next();
	     Integer noOfTimes = val_Occurences.get(value);
	     System.out.println("noOfTimes -"+noOfTimes+" arr-"+value);
	     sortedMap.put(noOfTimes,value);
	   }
	   for(Iterator reversIter = ((TreeMap) sortedMap).descendingKeySet().iterator();reversIter.hasNext(); )
	   {
	     Integer noOfTimes = (Integer)reversIter.next();
	     Integer value = sortedMap.get(noOfTimes);
	     System.out.println(value +" - "+noOfTimes);
	     for(int i=0;i<noOfTimes;i++)
	     {
	       //System.out.print(value+ ", ");
	     }
	   }
	 }
	 
	 public static void sortBasedOnOccurences2(int arr[])
	 {
	   Map<Integer,Integer> val_Occurences = new HashMap();
	   for(int i=0;i<arr.length;i++)
	   {
	     Integer noOfTimes = val_Occurences.get(arr[i]);
	     if(noOfTimes==null)
	     {
	     
	       val_Occurences.put(arr[i],1);
	     }
	     else
	     {
	       val_Occurences.put(arr[i],noOfTimes+1);
	     }
	   }
	   PriorityQueue sorList = new PriorityQueue(new FreqMapComparator());
	   sorList.addAll(val_Occurences.entrySet());

	   while(!sorList.isEmpty())
	   {
	     Map.Entry<Integer, Integer> val_Times = (Entry<Integer, Integer>) sorList.poll();
	     Integer value = val_Times.getKey();
	     Integer noOfTimes = val_Times.getValue();
	     //System.out.println(value +" - "+noOfTimes);
	     for(int i=0;i<noOfTimes;i++)
	     {
	       System.out.print(value+ ", ");
	     }
	   }
	 }
	 
	 
	 public static class Number{
		 int value;
		 int noOfTimes;
		 public Number(int val)
		 {
			 value = val;
			 noOfTimes = 1;
		 }
		public int getValue() {
			return value;
		}
		public void setValue(int value) {
			this.value = value;
		}
		public int getNoOfTimes() {
			return noOfTimes;
		}
		public void setNoOfTimes(int noOfTimes) {
			this.noOfTimes = noOfTimes;
		}
		@Override
		public boolean equals(Object obj)
		{
			boolean ret = false;
			if(obj==null ) ret = false;
			if(obj instanceof Number)
			{
				Number obj1 = (Number)obj;
				if(obj1.getValue() == this.getValue())
				{
					ret = true;
				}
			}
			return ret;
		}
		
		@Override 
		public int hashCode()
		{
			return Objects.hash(this.getValue()+31);
		}
	 }
	 
	
	 
	 public static class FreqMapComparator implements Comparator<Map.Entry<Integer, Integer>>{
			@Override
			public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
				int ret = 0;
				if(o1.getValue() == o2.getValue())
					ret = 0;
				else if(o1.getValue() > o2.getValue())
					ret = -1;
				else if(o1.getValue() < o2.getValue())
					ret =1;
				return ret;
			}
	 }
			
	
	 
	public static void main(String[] args) {
		int arr[] = {2, 2, 3, 5, 4, 3, 6, 3, 5, 23, 7};
		//int arr[] = { 3, 3, 3, 3};
		sortBasedOnOccurences2(arr);
	}

}
