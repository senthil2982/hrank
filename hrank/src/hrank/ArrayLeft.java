package hrank;

import java.util.Scanner;

public class ArrayLeft {

	public static int[] rotateArr(int arr[],int k)
	{
		int temp[] = new int[arr.length];

		for(int i = 0;i<arr.length;i++)
		{
			int newpos = (i + k)%arr.length;
			System.out.println(i +" "+ newpos);
			temp[i] = arr[newpos];
		
		}
		return temp;
	}
	
	public static void main(String[] args) {
		  Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int k = in.nextInt();
	        int a[] = new int[n];
	        for(int a_i=0; a_i < n; a_i++){
	            a[a_i] = in.nextInt();
	        }
	        int[] temp = rotateArr(a,k);
	        for(int i=0; i < n; i++){
	          System.out.print(temp[i]+" ");
	        }
	      

	}

}
