package hrank;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashRansom {

	private static  boolean isPrintable(Map<String,Integer> magz,String[] note)
	{
		boolean isPrint = false;
		for(int i=0;i<note.length;i++)
		{
			Integer count = magz.get(note[i]);
			if(count!=null && count > 0)
			{
				System.out.println("containskey "+note[i]+" count- "+magz.get(note[i]));
			
					magz.put(note[i], --count);
			}
			else
			{
					return isPrint;
			}
		}

		return true;
	}
	
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m = in.nextInt();
        int n = in.nextInt();
        String magazine[] = new String[m];
        Map<String,Integer> magzHash = new HashMap<String,Integer>();
        for(int magazine_i=0; magazine_i < m; magazine_i++){
            magazine[magazine_i] = in.next();
            if(magzHash.containsKey(magazine[magazine_i]))
            {
            	int count = magzHash.get(magazine[magazine_i]);
            	magzHash.put(magazine[magazine_i],++count);
            	
            }
            else
            {	
            magzHash.put(magazine[magazine_i],1);
            }
        }
        String ransom[] = new String[n];
        for(int ransom_i=0; ransom_i < n; ransom_i++){
            ransom[ransom_i] = in.next();
        }
        if(m<n)
        {
        	System.out.println("No");
        }
        else
        {
        if(isPrintable(magzHash,ransom))
        {
        	System.out.println("Yes");
        }
        else
        {
        	System.out.println("No");
        }
        }
    }

	
}
