package hrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HeapMedian {

	public static void main(String[] args) {
		Heap heap = new Heap();
		List minHp = new ArrayList();
		List maxHp = new ArrayList();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        for(int c:a)
        {
        	if( minHp.size()<=maxHp.size()) 
        		heap.insertMax(minHp, c);
        	else
        		heap.insertMin(maxHp, c);
        	heap.balance(minHp,maxHp);
        	System.out.println(heap.getMedian(minHp, maxHp));
        }
    }

}

class Heap
{
	
	public int left(int idx)
	{
		return (2*idx)+1;
	}
	public int right(int idx)
	{
		return (2*idx)+2;
	}
	
	public int parent(int idx)
	{
		if (idx==0) return 0;
		return (idx-1)/2;
	}
	
	private void swap(List<Integer> list,int i,int j)
	{
		int temp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, temp);
	}
	
	public void insertMin(List<Integer> list,int val)
	{
		list.add(val);
		int index = list.size() - 1;
		minheapifyUp(list,index);
	}
	
	public void insertMax(List<Integer> list,int val)
	{
		list.add(val);
		int index = list.size() - 1;
		maxheapifyUp(list,index);
	}
	
	public void balance(List<Integer> low,List<Integer> high)
	{
		 while(!low.isEmpty() && !high.isEmpty() && low.get(0) > high.get(0)) {
	            Integer lowHead= minpoll(low);
	            Integer highHead = maxpoll(high);
	            insertMin(low,highHead);
	            insertMax(high,lowHead);
	        }
		 System.out.print("maxHp - ");
			printAll(high);
			System.out.print("minHp - ");
			printAll(low);
	}
	
	private int minpoll(List<Integer> list)
	{
		int root = 0;
		if(list.size()>0)
		{
			root = list.get(0);
			list.set(0, list.get(list.size()-1));
			list.remove(list.size()-1);
			minheapifyDown(list,list.size(),0);
		}
		return root;
	}
	private int maxpoll(List<Integer> list)
	{
		int root = 0;
		if(list.size()>0)
		{
			root = list.get(0);
			list.set(0, list.get(list.size()-1));
			list.remove(list.size()-1);
			maxheapifyDown(list,list.size(),0);
		}
		return root;
	}
	private void minheapifyDown(List<Integer> list,int n, int i)
	{
		int left = left(i);
		int right = right(i);
		int smallest = i;
		if(left < n && list.get(left)<list.get(i) )
			smallest = left;
		
		if(right < n && list.get(right) < list.get(smallest))
			smallest = right;
		
		if(smallest!=i)
		{
			swap(list,i,smallest);
			minheapifyDown(list,n,smallest);
		}
	}
	
	private void maxheapifyDown(List<Integer> list,int n, int i)
	{
		int left = left(i);
		int right = right(i);
		int largest = i;
		if(left < n && list.get(left)>list.get(i) )
			largest = left;
		
		if(right < n && list.get(right) > list.get(largest))
			largest = right;
		
		if(largest!=i)
		{
			swap(list,i,largest);
			maxheapifyDown(list,n,largest);
		}
	}
	
	private void minheapifyUp(List<Integer> list,int i)
	{
		while(i!=0 && list.get(parent(i)) > list.get(i))
		{
			swap(list,i,parent(i));
			i = parent(i);
		}
	}
	
	private void maxheapifyUp(List<Integer> list,int i)
	{
		while(i!=0 && list.get(parent(i)) < list.get(i))
		{
			swap(list,i,parent(i));
			i = parent(i);
		}
	}
	
	private void printAll(List<Integer> list)
	{
		for(int i:list)
		{
			System.out.print(i+" ");
		}
		System.out.println("");
	}
	
	
	public double getMedian(List<Integer> low,List<Integer> high)
	{
				 if(low.isEmpty() && high.isEmpty()) {
	            throw new IllegalStateException("Heap is empty");
	        } else {
	            return low.size() == high.size() ? (low.get(0) + high.get(0)) / 2.0 : low.get(0);
	        }
	
	}
	
}