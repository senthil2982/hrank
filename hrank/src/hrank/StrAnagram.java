package hrank;

import java.util.Scanner;

public class StrAnagram {

	 public static int numberNeeded(String first, String second) {
	      int arr[]  = new int[26];
	      int count = 0;
	        for(char c: first.toCharArray())
	        {
	        	arr[c-'a']++;
	        }
	        for(char c: second.toCharArray())
	        {
	        	arr[c-'a']--;
	        }
	        for(int i=0;i<arr.length;i++)
	        {
	        	count+=Math.abs(arr[i]);
	        }
	        return count;
	    }
	  
	    public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        String a = in.next();
	        String b = in.next();
	        System.out.println(numberNeeded(a, b));
	    }

}
