package hrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class TrieImp {
	
/*
	public static void main(String[] args) {
	      Trie dict = new Trie();
	        dict.insert("are");
	        dict.insert("area");
	        dict.insert("areve");
	        dict.insert("base");
	        dict.insert("cat");
	        dict.insert("cater");
	        
	        dict.insert("basement");
	 
	        String input = "are";
	        System.out.print(input + ":   ");
	        System.out.println(dict.countMatchingPrefix(input));

	}
*/
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Trie dict = new Trie();
        List<Integer> res = new ArrayList<Integer>();
        for(int a0 = 0; a0 < n; a0++){
            String op = in.next();
            String contact = in.next();
            if(op.equals("add"))
            {
            	dict.insert(contact);
            }
            else if(op.equals("find"))
            {
            	res.add(dict.countMatchingPrefix(contact));
            }
        }
        for(int val:res)
        {
        	System.out.println(val);
        }
    }
}

class Trie{
	boolean isEnd;
	char c;
	HashMap<Character,Trie> children;
	Trie root;
	int size;
	public Trie()
	{
		root = new Trie((char)0);
	}
	public Trie(char ch)
	{
		c = ch;
		children = new HashMap<Character,Trie>();
		isEnd =  false;
	}
	
	public void insert(String word)
	{
		int length = word.length();
		Trie crawl = root;
		for(int level = 0;level< length;level++)
		{
			HashMap<Character,Trie> child = crawl.children;
			char ch = word.charAt(level);
			if(child.containsKey(ch))
			{
				crawl = child.get(ch);
			}
			else
			{
				Trie newNode = new Trie(ch);
				child.put(ch, newNode);
				crawl = newNode;
			}
			crawl.size++;
		}
		crawl.isEnd = true;
	}
	
	public String getMatchingPrefix(String word)
	{
		Trie crawl = root;
		String result = "";
		int prevMatch = 0;
		for(int level = 0;level< word.length();level++)
		{
			HashMap<Character,Trie> child = crawl.children;
			char ch = word.charAt(level);
			if(child.containsKey(ch))
			{
				result+=ch;
				crawl = child.get(ch);
				if(crawl.isEnd)
				{
					prevMatch = level+1;
				}
			}
			else
				break;
		}
		if(!crawl.isEnd)
			return result.substring(0,prevMatch);
		return result;
	}
	
	public int countMatchingPrefix(String word)
	{
		Trie crawl = root;
		int count = 0;
		for(int level = 0;level < word.length();level++)
		{
			HashMap<Character,Trie> child = crawl.children;
			char ch = word.charAt(level);
			crawl = child.get(ch);
			if(crawl==null) return 0;
		}
		return crawl.size;
	}
	
}
