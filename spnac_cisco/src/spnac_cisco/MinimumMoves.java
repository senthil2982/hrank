package spnac_cisco;

public class MinimumMoves {

	public static void main(String[] args) {
		int arr1[]={1234,4321};
		int arr2[]={2345,3214};
		System.out.println(minimumMoves(arr1,arr2));
	}
	
	 static int minimumMoves(int[] a, int[] m) {
		 int noOfOps = 0;
		 for(int i=0;i<a.length;i++)
		 {
			 noOfOps += findOps(a[i],m[i]);
		 }
		 return noOfOps;
	    }
	 
	 static int findOps(int a, int m) {
		 String number1 = String.valueOf(a);
		 String number2 = String.valueOf(m);
		 int noofops = 0;
		 for(int i = 0; i < number1.length(); i++) {
		     int j = Character.digit(number1.charAt(i), 10);
		     int k = Character.digit(number2.charAt(i), 10);
		 noofops+=Math.abs(j-k);
	 }
		 return noofops;
	 }
}
