package spnac_cisco;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SubArrSum {

	  public static void main(String[] args) throws IOException{
	        
	        int _arr[] = {1,2,3,4};
	        long res = subarraySum(_arr);
	        System.out.println(res);
	        long res1 = subarraySum1(_arr);
	        System.out.println(res1);
	    }
	
static long subarraySum(int[] arr) {

    long sum = 0;
 
     for (int i=0; i<arr.length; i++)
        sum+= (arr[i] * (i+1) * (arr.length-i));
   
    return sum ;
}

static long subarraySum1(int[] arr) {
long  sum = 0;

for (int i=0; i < arr.length; i++)
{
    for (int j=i; j<arr.length; j++)
    {
        for (int k=i; k<=j; k++)
        {
        	System.out.print(arr[k]+" ");
            sum += arr[k] ;
        }
        System.out.println();
    }
}
return sum ;
}
}
